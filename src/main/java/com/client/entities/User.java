package com.client.entities;



import javax.ws.rs.FormParam;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class User {
    private int id;
    @FormParam("userName")
    private String userName;
   @FormParam("desc")
    private String desc;
    public User(){
        super();
    }
    public User(int id, String userName){
        this.id=id;
        this.userName=userName;
    }
    public User(int id, String userName,String desc){
        this.id=id;
        this.userName=userName;
        this.desc=desc;
    }

    @XmlElement
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    @XmlElement
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    @XmlElement
    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}